// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'Main page elements': function (browser) {
    browser
    .url('http://localhost:8080')
      .waitForElementVisible('#app', 5000)
      .assert.containsText('h1', 'Ginger payments assignments')
      .assert.elementCount('h2', 4)
      .assert.elementCount('p', 5)
      .end()
  },

  'Callback button': function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('#paymentTableContainer', 1000)
      .assert.containsText('#paymentTableContainer > button', 'Load top 20')
      .click('#paymentTableContainer > button')
      .waitForElementVisible('#paymentTableContainer > table', 2000)
      .assert.elementCount('#paymentTableContainer > table > thead > tr > th', 7)
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(1)', 'ID')
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(2)', 'Method')
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(3)', 'Amount')
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(4)', 'Currency')
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(5)', 'Created')
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(6)', 'Status')
      .assert.containsText('#paymentTableContainer > table > thead > tr > th:nth-child(7)', 'Merchant')
      .assert.elementCount('#paymentTableContainer > table > tbody > tr', 20)
      .end()
  },

  'Promise button': function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('#filterTable', 1000)
      .assert.containsText('#filterTable > select', 'Ginger')
      .assert.containsText('#filterTable > button', 'Get payments for Ginger')
      .click('#filterTable > button')
      .waitForElementVisible('#filterTable > table', 1000)
      .assert.elementCount('#filterTable > table > thead > tr > th', 7)
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(1)', 'ID')
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(2)', 'Method')
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(3)', 'Amount')
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(4)', 'Currency')
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(5)', 'Created')
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(6)', 'Status')
      .assert.containsText('#filterTable > table > thead > tr > th:nth-child(7)', 'Merchant')
      .end()
  },

  'Filter payment method': function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('#filterPaymentMethodTable', 1000)
      .assert.containsText('#filterPaymentMethodTable > select', 'Select a payment method...')
      .click('#filterPaymentMethodTable > select > option[value="ideal"]')
      .waitForElementVisible('#filterPaymentMethodTable > table', 1000)
      .assert.elementCount('#filterPaymentMethodTable > table > thead > tr > th', 7)
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(1)', 'ID')
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(2)', 'Method')
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(3)', 'Amount')
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(4)', 'Currency')
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(5)', 'Created')
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(6)', 'Status')
      .assert.containsText('#filterPaymentMethodTable > table > thead > tr > th:nth-child(7)', 'Merchant')
      .assert.containsText('#filterPaymentMethodTable > table > tbody > tr > td:nth-child(2)', 'ideal')
      .click('#filterPaymentMethodTable > select > option[value="creditcard"]')
      .waitForElementVisible('#filterPaymentMethodTable > table', 1000)
      .assert.containsText('#filterPaymentMethodTable > table > tbody > tr > td:nth-child(2)', 'creditcard')
      .click('#filterPaymentMethodTable > select > option[value="bank-transfer"]')
      .waitForElementVisible('#filterPaymentMethodTable > table', 1000)
      .assert.containsText('#filterPaymentMethodTable > table > tbody > tr > td:nth-child(2)', 'bank-transfer')
      .end()
  },
  'Add Payments': function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('#addPaymentForm', 1000)
      .assert.containsText('#addPaymentForm > form > fieldset > legend', 'Add a payment:')
      .assert.containsText('#addPaymentForm > form > fieldset > label[for="currency"]', 'Currency')
      .assert.elementPresent('#addPaymentForm > form > fieldset > select#currency')
      .assert.containsText('#addPaymentForm > form > fieldset > label[for="amount"]', 'Amount (in cents)')
      .assert.elementPresent('#addPaymentForm > form > fieldset > input[type="number"]#amount')
      .assert.containsText('#addPaymentForm > form > fieldset > label[for="method"]', 'Payment method')
      .assert.elementPresent('#addPaymentForm > form > fieldset > select#method')
      .assert.containsText('#addPaymentForm > form > fieldset > label[for="merchant"]', 'Merchant')
      .assert.elementPresent('#addPaymentForm > form > fieldset > input[type="text"]#merchant')
      .assert.elementPresent('#addPaymentForm > form > fieldset > input[type="submit"]')
      .assert.value('#addPaymentForm > form > fieldset > input[type="submit"]', 'Add payment')
      .end()
  },
  'Add payments - Submit form': function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('#addPaymentForm', 1000)
      .click('#addPaymentForm > form > fieldset > select#currency > option[value="EUR"]')
      .setValue('#addPaymentForm > form > fieldset > input[type="number"]#amount', '1234567890')
      .click('#addPaymentForm > form > fieldset > select#method > option[value="ideal"]')
      .setValue('#addPaymentForm > form > fieldset > input[type="text"]#merchant', 'ACME CORP')
      .click('#addPaymentForm > form > fieldset > input[type="submit"]')
      .pause(500)
      .assert.value('#addPaymentForm > form > fieldset > input[type="submit"]', 'Saved! Add another payment')
      .end()
  },

  'Add payments - Submit form with empty fields': function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('#addPaymentForm', 1000)
      .click('#addPaymentForm > form > fieldset > select#currency > option[value="EUR"]')
      .click('#addPaymentForm > form > fieldset > select#method > option[value="ideal"]')
      .click('#addPaymentForm > form > fieldset > input[type="submit"]')
      .pause(500)
      .assert.value('#addPaymentForm > form > fieldset > input[type="submit"]', 'Fill in all the fields then try again!')
      .end()
  }
}
