# ginger-payments

## Build Setup
next to all the extra's there are also 60 e2e tests present, hope you will like it!

First run the json-server and cd into to clone, then run:
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# for the e2e tests run:
npm run e2e

```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
