import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'

Vue.use(VueResource)
Vue.http.options.root = 'http://localhost:3000/payments'

/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App }
})
